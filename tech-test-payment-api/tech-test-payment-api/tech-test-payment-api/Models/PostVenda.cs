﻿namespace tech_test_payment_api.Models
{
    public class PostVenda
    {
        public Vendedor Vendedor { get; set; }
        public List<Produto> Produtos { get; set; }

        public PostVenda(Vendedor vendedor, List<Produto> produtos)
        {
            Vendedor = vendedor;
            Produtos = produtos;
        }
    }
}
