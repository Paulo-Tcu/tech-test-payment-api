﻿using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        [Required]
        [MaxLength(250)]
        public string Nome { get; set; }

        [Required]
        public string Cpf { get; set; }

        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        [Required]
        public string Telefone { get; set; }

        public Vendedor(string nome, string cpf, string email, string telefone)
        {
            if (!Util.IsCpf(cpf))
                throw new BadHttpRequestException("CPF inválido", 400);

            if (!Util.IsEmail(email))
                throw new BadHttpRequestException("E-mail inválido",400);

            if (!Util.IsTelefone(telefone))
                throw new BadHttpRequestException("Telefone inválido",400);

            if(string.IsNullOrEmpty(nome))
                throw new BadHttpRequestException("Nome inválido",400);

            Nome = nome.ToUpper().Trim();
            Cpf = Util.FormatCpf(cpf);
            Email = email.Trim();
            Telefone = Util.FormatTelefone(telefone);
        }
    }
}
