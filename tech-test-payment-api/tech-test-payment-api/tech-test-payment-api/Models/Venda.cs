﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using tech_test_payment_api.Enum;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public Guid Id { get; }

        [Required]
        [JsonPropertyName("Vendedor")]
        public Vendedor DadosVendedor { get; private set; }

        [Required]
        public List<Produto> Produtos { get; private set; }

        [JsonPropertyName("Status da venda")]
        public Status Status { get; private set; }

        [JsonPropertyName("Data da venda")]
        public DateTime DataVenda { get; private set; }

        public Venda(Vendedor vendedor, List<Produto> produtos)
        {
            Id = Guid.NewGuid();
            DadosVendedor = vendedor;
            Produtos = produtos;
            Status = Status.Aguardando_pagamento;
            DataVenda = Convert.ToDateTime(DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
        }

        /// <summary>
        /// Verifica se Status recebido atende as regras de negócio e altera propriedade da classe.
        /// </summary>
        /// <param name="newStatus">Novo Status</param>
        /// <exception cref="ArgumentException">Retorna uma exceção por não atender os critérios da regra de negócio.</exception>
        public void SetStatus(Status newStatus)
        {
            switch (newStatus)
            {
                case Status.Pagamento_aprovado:
                    if (Status == Status.Aguardando_pagamento)
                        Status = newStatus;

                    else
                        throw new ArgumentException($"Não é possível alterar de {Status.ToString().Replace("_", " ")} " +
                            $"para {newStatus.ToString().Replace("_", " ")}", nameof(newStatus));
                    break;

                case Status.Cancelada:
                    if (Status == Status.Aguardando_pagamento || Status == Status.Pagamento_aprovado)
                        Status = newStatus;

                    else
                        throw new ArgumentException($"Não é possível alterar de {Status.ToString().Replace("_", " ")} " +
                            $"para {newStatus.ToString().Replace("_", " ")}", nameof(newStatus));
                    break;

                case Status.Enviado_para_transportadora:
                    if (Status == Status.Pagamento_aprovado)
                        Status = newStatus;

                    else
                        throw new ArgumentException($"Não é possível alterar de {Status.ToString().Replace("_", " ")} " +
                            $"para {newStatus.ToString().Replace("_", " ")}", nameof(newStatus));
                    break;

                case Status.Entregue:
                    if (Status == Status.Enviado_para_transportadora)
                        Status = newStatus;

                    else
                        throw new ArgumentException($"Não é possível alterar de {Status.ToString().Replace("_", " ")} " +
                            $"para {newStatus.ToString().Replace("_", " ")}", nameof(newStatus));
                    break;

                default:
                    throw new ArgumentException($"O status {Status.Aguardando_pagamento.ToString().Replace("_", " ")} é " +
                        $"atribuido na criação da venda", nameof(newStatus));
            }
            
        }
    }
}
