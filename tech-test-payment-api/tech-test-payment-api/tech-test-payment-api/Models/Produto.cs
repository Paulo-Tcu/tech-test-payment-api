﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class Produto
    {
        [Required]
        [MaxLength(100)]
        public string Descricao { get; set; }

        [Required]
        public decimal Valor { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Quantidade { get; set; }

        public Produto(string descricao, decimal valor, int quantidade)
        {
            Descricao = descricao;
            Valor = Math.Round(valor, 2);
            Quantidade = quantidade;

        }
    }
}
