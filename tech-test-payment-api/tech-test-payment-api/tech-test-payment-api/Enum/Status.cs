﻿using System.Text.Json.Serialization;

namespace tech_test_payment_api.Enum
{
    public enum Status
    {
        Aguardando_pagamento,
        Pagamento_aprovado,
        Enviado_para_transportadora,
        Entregue,
        Cancelada
    }
}
