﻿using System.Text.RegularExpressions;

namespace tech_test_payment_api
{
    public static class Util
    {

        //Classe criada para armazenar funções e metódos genéricos em sua aplicação que podem ser úteis e reutilizadas durante o código
        
        /// <summary>
        /// Recebe uma string e verifica se corresponde a um CPF válido
        /// </summary>
        /// <param name="cpf">CPF a ver verificado</param>
        /// <returns>Retorna Verdadeiro ou Falso</returns>
        public static bool IsCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim().Replace(".", "").Replace("-", "");

            if (cpf.Length != 11 || cpf == string.Concat(Enumerable.Repeat("1", 11)))
                return false;

            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

            resto = soma % 11;

            if (resto < 2)
                resto = 0;

            else
                resto = 11 - resto;

            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;

            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

            resto = soma % 11;

            if (resto < 2)
                resto = 0;

            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            return cpf.EndsWith(digito);
        }

        /// <summary>
        /// Recebe uma string e verifica se corresponde a um e-mail válido
        /// </summary>
        /// <param name="email">E-mail a ser verificado</param>
        /// <returns>Verdadeiro ou Falso</returns>
        public static bool IsEmail(string email)
        {
            var regex = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

            return email != null && !string.IsNullOrEmpty(email) ? regex.IsMatch(email.Trim()) : false;
        }

        /// <summary>
        /// Recebe uma string e verifica se corresponde a um e-mail válido
        /// </summary>
        /// <param name="telefone">Telefone a ser verificado</param>
        /// <returns>Verdadeiro ou Falso</returns>
        public static bool IsTelefone(string telefone)
        {
            var regex = new Regex("^[1-9]{2}(?:[2-8]|9[1-9])[0-9]{3}[0-9]{4}$");

            return telefone != null && !string.IsNullOrEmpty(telefone) ? 
                regex.IsMatch(telefone.Trim().Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "")) 
                : false;
        }

        /// <summary>
        /// Recebe um string telefone e formata no padrão (DDD)99999-9999 ou (DDD)9999-9999 caso telefone fixo
        /// </summary>
        /// <param name="telefone">Telefone a ser formatado</param>
        /// <returns>String no formato mencionado da introdução</returns>
        public static string FormatTelefone(string telefone)
        {
            telefone = telefone.Trim().Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "");

            return "(" + telefone.Substring(0, 2) + ")" + telefone.Substring(2, telefone.Length - 6) + "-" + telefone.Substring(telefone.Length - 4);
        }

        /// <summary>
        /// Recebe um string CPF e formata no 999.999.999-99
        /// </summary>
        /// <param name="cpf">CPF a ser formatado</param>
        /// <returns>String no formato mencionado da introdução</returns>
        public static string FormatCpf(string cpf)
        {
            cpf = cpf.Trim().Replace(".", "").Replace("-", "");

            return cpf.Substring(0, 3) + "." + cpf.Substring(3, 3) + "." + cpf.Substring(6, 3) + "-" + cpf.Substring(9);
        }
    }
}
