﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Enum;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendaController : ControllerBase
    {
        /// <summary>
        /// Propriedade para uso somente interno (privada),estática para ser alterada e consultada a cada request solicitado
        /// </summary>
        private static List<Venda> _vendasRegistradas;

        /// <summary>
        /// Método que consome dados recebidos por POST e cria uma venda com ID único
        /// </summary>
        /// <param name="vendaRequest">Dados necessários para se criar uma nova venda</param>
        /// <returns>OK -> Retorna dados da venda. BadRequest ->  retorno será um obj anonimo com msg que não foi possível realizar requisição e motivo</returns>
        [HttpPost("CreateVenda")]
        public IActionResult CreateVenda(PostVenda vendaRequest)
        {
            try
            {
                if(vendaRequest.Produtos.Count() == 0)
                    return BadRequest(new {erro = "A lista de produtos/itens deve possuir no minímo 1 (um) item", code = 400});

                var venda = new Venda(vendaRequest.Vendedor, vendaRequest.Produtos);

                if(_vendasRegistradas != null)
                    _vendasRegistradas.Add(venda);

                else
                    _vendasRegistradas = new List<Venda> {venda};

                return Ok(venda);
            }
            catch(ArgumentException e)
            {
                return BadRequest(new {erro = e.Message,paramName = e.ParamName, code = 400});
            }
            catch(Exception e)
            {
                return BadRequest(new {erro = e.Message, code = 400});
            }
        }

        /// <summary>
        /// Utilizado para devolver os dados da venda solicitada.
        /// </summary>
        /// <param name="id">ID da venda</param>
        /// <returns>Retorna dados da venda ou NotFound caso não encontrá-la</returns>
        [HttpGet("GetVenda")]
        public IActionResult GetVenda(Guid id)
        {
            var venda = _vendasRegistradas.Where(w => w.Id == id).FirstOrDefault();

            if (venda == null)
                return NotFound();

            return Ok(venda);
        }

        /// <summary>
        /// Utilizado para alterar status de uma venda.
        /// </summary>
        /// <param name="id">ID da venda</param>
        /// <param name="status">Novo status</param>
        /// <returns>Retorna os dados da venda após atualizado, caso contrário retorna erro e motivo de não conseguir concluir operação.</returns>
        [HttpPut("UpdateStatus")]
        public IActionResult UpdateVenda(Guid id, Status status)
        {
            var venda = _vendasRegistradas.Where(w => w.Id == id).FirstOrDefault();

            if (venda == null)
                return NotFound();

            try
            {
                venda.SetStatus(status);

                return CreatedAtAction(nameof(GetVenda), new {id = venda.Id});
            }
            catch(ArgumentException e)
            {
                return BadRequest(new { erro = e.Message, paramName = e.ParamName, code = 400 });
            }
            catch (Exception e)
            {
                return BadRequest(new { erro = e.Message, code = 400 });
            }
        }
    }
}
